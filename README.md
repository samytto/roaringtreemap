RoaringTreeMap for 64-bit integers
=============

This bitmap's scheme is conceived to efficiently represents 64-bit integers. This model is simply to implement. It adopts a Java TreeMap on the first level. Each node of tree is composed of a 32-bit key and a pointer to a Roaring bitmap. The 32-bit key is used to store the shared high 32 bits of a group of integers, for which the remaining 32 bits are conserved on the associated Roaring bitmap.

License
------
This code is licensed under Apache License, Version 2.0 (ASL2.0).     

Usage
------

* Get java
* Get maven 2

* mvn compile will compile
* mvn test will run the unit tests
* mvn package will package in a jar (found in target)

Funding 
----------

This work was supported by NSERC grant number 26143.
