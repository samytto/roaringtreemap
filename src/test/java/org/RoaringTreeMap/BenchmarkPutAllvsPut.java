package org.RoaringTreeMap;

import java.util.Iterator;
import java.util.Map.Entry;

import org.RoaringTreeMap.Tree;
import org.roaringbitmap.RoaringBitmap;


public class BenchmarkPutAllvsPut {
	
	public static void main(String[] args) {
		//###########################################################
		System.out.println("Testing with a big array");
		//###########################################################
		DataGenerator dgBig = new   DataGenerator(1000000);		
		long[] bigArray = dgBig.getZipfian();
		int TIMES=100, warmUp = 1000;
		long bef=0, aft=0;
		Tree trBig = new Tree();
		//Trees construction
		for(int i=0; i<bigArray.length; i++)
			trBig.add(bigArray[i]);
		System.out.println("Nb mappings big tree = "+trBig.getTree().entrySet().size());
		long putAllTime=0, putTime=0, go=0;
		for(int i=0; i<warmUp+TIMES; i++){
			bef = System.currentTimeMillis();
			Tree putAllTr = new Tree();
			putAllTr.putAll(trBig);
			aft = System.currentTimeMillis();
			if(i>=warmUp)
				putAllTime+=aft-bef;
			go+=putAllTr.getCardinality();
			bef = System.currentTimeMillis();
			Tree putTr = new Tree();
			Iterator<Entry<Integer, RoaringBitmap>> it = trBig.getTree().entrySet().iterator();
			while(it.hasNext()){
				Entry<Integer, RoaringBitmap> e = it.next();
				putTr.getTree().put(new Integer(e.getKey()), e.getValue().clone());
			}
			aft = System.currentTimeMillis();
			if(i>=warmUp)
				putTime+=aft-bef;
			go+=putTr.getCardinality();
		}
		System.out.println("PutAll average time = "+putAllTime/TIMES+" ms");		
		System.out.println("Put average time = "+putTime/TIMES+" ms");
		System.out.println("Ignore = "+go);		
	}
}
