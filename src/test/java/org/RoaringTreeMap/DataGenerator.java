package org.RoaringTreeMap;

/*
 * (c) Samy Chambi and daniel Lemire.
 */

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

/**
 * 
 * This is a data generator object reproducing the models used
 * by Colantonio and Di Pietro, Concise: Compressed n Composable Integer Set
 * 
 * @author Daniel Lemire
 *
 */
public class DataGenerator {
        private Random rand = new Random();
        private int N;
        private long max = ((long)1<<63)-1;
        boolean zipfian = false;

        /**
         * Will generate arrays with default size (100000)
         */
        public DataGenerator() {
                N = 100000;
        }
        
        /**
         * @param n size of the arrays
         */
        public DataGenerator(final int n) {
                if(n<1) throw new IllegalArgumentException("number of ints should be positive");
                N = n;
        }
        
        /**
         * @return whether the data generator is in zipfian mode
         */
        public final boolean is_zipfian() {
                return zipfian;
        }
        
        /**
         * set the data generator in uniform  mode
         */
        public final void setUniform() {
                zipfian = false;
        }
        
        /**
         * set the data generator in Zipfian mode
         */
        public final void setZipfian() {
                zipfian = true;
        }
        
        public int getN() {
        	return N;
        }
        
        public Random getRand() {
        	return rand;
        }                
        
        /**
         * Generate a random array (sorted integer set)
         * 
         * @param d should vary from 0 to 1.000
         * @return an array with a uniform distribution
         */
        public final long[] getUniform() {
               
                final HashSet<Long> hash = new HashSet<Long>();   
                while(hash.size()<N) {
                     final double a = rand.nextDouble();
                     final long x = (long) Math.floor(a * max);
                     hash.add(x);
                }               
                int c = 0;
                long[] answer = new long[N];
                for(long x : hash)
                   answer[c++] = x;
                Arrays.sort(answer);                
                return answer;
        }
        
        /**
         * Generate a random array (sorted integer set). 
         * 
         * This is what Colantonio and Di Pietro called Zipfian.
         *
         */
        public final long[] getZipfian() {
                HashSet<Long> hash = new HashSet<Long>();
                int loopcount = 0;
                while(hash.size()<N) {
                        final double a = rand.nextDouble();
                        final long x = (long) Math.floor(a*a * max);
                        hash.add(x);
                        if(loopcount++ > 10*N) throw new RuntimeException("zipfian generation is too expensive");
                }
                int c = 0;
                long[] answer = new long[N];
                for(long x : hash)
                   answer[c++] = x;
                Arrays.sort(answer);
                return answer;
        }
        
        public static void main(final String[] args) {
        	DataGenerator dg = new DataGenerator();
        	long v1[] = dg.getUniform();//1<<24
        	long v2[] = dg.getZipfian();
        	//long v3[] = DataGenerator.getChunkedUniform(0.0001, 1<<24);
        	PrintWriter pw1=null, pw2=null, pw3=null;
        	System.out.println("Making files");
        	try {
				pw1 = new PrintWriter("Array1","UTF-8");
				pw2 = new PrintWriter("Array2","UTF-8");
				pw3 = new PrintWriter("Array3","UTF-8");
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
        	pw1.println("Printing first level v1 uniform :");
        	for(int i=0; i<v1.length; i++)
        		pw1.print((v1[i]>>32)+", ");
        	//pw1.println("\nPrinting long integers v1 :");
        	//for(int i=0; i<v1.length; i++)
        		//pw1.print(v1[i]+", ");
        	pw1.close();        	
        	pw2.println("Printing first level v2 zipf :");
        	for(int i=0; i<v2.length; i++)
        		pw2.print((v2[i]>>32)+", ");        	
        	//pw2.println("Printing long integers v2 :");
        	//for(int i=0; i<v2.length; i++)
        		//pw2.print(v2[i]+", ");
        	pw2.close();        	
        	/*pw3.println("Printing first level v3 zipf :");
        	for(int i=0; i<v3.length; i++)
        		pw3.print(((v3[i]>>32)&0xFFFFFFFF)+", ");        	
        	pw3.println("Printing integers v3 :");
        	for(int i=0; i<v3.length; i++)
        		pw3.print(v3[i]+", ");
        	pw3.close();*/
        }
}