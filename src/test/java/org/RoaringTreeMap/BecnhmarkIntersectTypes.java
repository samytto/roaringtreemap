package org.RoaringTreeMap;

import org.RoaringTreeMap.Tree;

public class BecnhmarkIntersectTypes {

	public static void main(String[] args) {
		//###########################################################
		System.out.println("Testing with a small and a big array");
		//###########################################################
		DataGenerator dgBig = new   DataGenerator(1000000);
		DataGenerator dgSmall = new DataGenerator(10000);		
		long[] bigArray = dgBig.getZipfian();
		long[] smallArray = dgSmall.getZipfian();
		int TIMES=100, warmUp = 1000;
		long bef=0, aft=0;
		Tree trBig = new Tree();
		Tree trSmall=new Tree();
		//Trees construction
		for(int i=0; i<bigArray.length; i++)
			trBig.add(bigArray[i]);
		for(int i=0; i<smallArray.length; i++)
			trSmall.add(smallArray[i]);
		System.out.println("Nb mappings big tree = "+trBig.getTree().entrySet().size());
		System.out.println("Nb mappings small tree = "+trSmall.getTree().entrySet().size());
		long linearitmicTime=0, linearTime=0, go=0;
		for(int i=0; i<warmUp+TIMES; i++){
			bef = System.currentTimeMillis();
			Tree result = Tree.linearithmicIntersect(trSmall, trBig);
			aft = System.currentTimeMillis();
			if(i>=warmUp)
				linearitmicTime+=aft-bef;
			go+=result.getCardinality();
			bef = System.currentTimeMillis();
			Tree result2 = Tree.linearIntersect(trBig, trSmall);
			aft = System.currentTimeMillis();
			if(i>=warmUp)
				linearTime+=aft-bef;
			go+=result2.getCardinality();
		}
		System.out.println("LinearitmicIntersect average time = "+linearitmicTime/TIMES+" ms");		
		System.out.println("LinearIntersect average time = "+linearTime/TIMES+" ms");
		System.out.println("Ignore = "+go);		
		
		//###########################################################
		System.out.println();
		System.out.println("Testing with same sizes arrays");
		System.out.println();
		//###########################################################
				dgBig =   new DataGenerator(1000000);
				dgSmall = new DataGenerator(1000000);		
				bigArray = dgBig.getUniform();
				smallArray = dgSmall.getUniform();				
				trBig = new Tree();
				trSmall=new Tree();
				//Trees construction
				for(int i=0; i<bigArray.length; i++)
					trBig.add(bigArray[i]);
				for(int i=0; i<smallArray.length; i++)
					trSmall.add(smallArray[i]);
				linearitmicTime=0; 
				linearTime=0; 
				go=0;
				for(int i=0; i<warmUp+TIMES; i++){
					bef = System.currentTimeMillis();
					Tree result = Tree.linearithmicIntersect(trSmall, trBig);
					aft = System.currentTimeMillis();
					if(i>warmUp)
						linearitmicTime+=aft-bef;
					go+=result.getCardinality();
					bef = System.currentTimeMillis();
					Tree result2 = Tree.linearIntersect(trBig, trSmall);
					aft = System.currentTimeMillis();
					if(i>warmUp)
						linearTime+=aft-bef;
					go+=result2.getCardinality();
				}
				System.out.println("LinearitmicIntersect average time = "+linearitmicTime/TIMES+" ms");		
				System.out.println("LinearIntersect average time = "+linearTime/TIMES+" ms");
				System.out.println("Ignore = "+go);
	}
}
