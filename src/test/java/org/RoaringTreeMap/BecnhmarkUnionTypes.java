package org.RoaringTreeMap;

import org.RoaringTreeMap.Tree;

public class BecnhmarkUnionTypes {

	public static void main(String[] args) {
		int[] sizes = {1000, 10000, 100000, 10000000, 100000000};
		for(int s=0; s<sizes.length; s++) 
			for (int k=s; k<sizes.length; k++) {
				int sizeSmall = sizes[s], sizeBig= sizes[k];
				System.out.println();
				System.out.println("########################################");				
				System.out.println("bigSize = "+sizeBig+", smallSize= "+sizeSmall);		
				System.out.println("########################################");
				System.out.println();
				DataGenerator dgBig = new   DataGenerator(sizeBig);
				DataGenerator dgSmall = new DataGenerator(sizeSmall);		
				long[] bigArray 	= dgBig.getUniform();
				long[] smallArray 	= dgSmall.getUniform();
				int TIMES=100, warmUp = 1000;
				long bef=0, aft=0;
				Tree trBig = new Tree();
				Tree trSmall=new Tree();
				//Trees construction
				for(int i=0; i<bigArray.length; i++)
					trBig.add(bigArray[i]);
				for(int i=0; i<smallArray.length; i++)
					trSmall.add(smallArray[i]);
				long linearitmicTime=0, linearTime=0, linearWithArrayTime=0, go=0;
				for(int i=0; i<warmUp+TIMES; i++){
					bef = System.currentTimeMillis();
					Tree result = Tree.linearithmicUnion(trSmall, trBig);
					aft = System.currentTimeMillis();
					if(i>=warmUp)
						linearitmicTime+=aft-bef;
					go+=result.getCardinality();
					bef = System.currentTimeMillis();
					Tree result2 = Tree.linearUnion(trSmall, trBig);
					aft = System.currentTimeMillis();
					if(i>=warmUp)
						linearTime+=aft-bef;
					go+=result2.getCardinality();
					bef = System.currentTimeMillis();
					Tree result3 = Tree.linearUnionWithArray(trSmall, trBig);
					aft = System.currentTimeMillis();
					if(i>=warmUp)
						linearWithArrayTime+=aft-bef;
					go+=result3.getCardinality();
				}		
				System.out.println("LinearithmicUnion average time = "+(linearitmicTime/TIMES)+" ms");		
				System.out.println("LinearUnion average time = "+(linearTime/TIMES)+" ms");
				System.out.println("LinearUnionWithArray average time = "+(linearWithArrayTime/TIMES)+" ms");
				System.out.println("Ignore = "+go);						
			}
	}
}