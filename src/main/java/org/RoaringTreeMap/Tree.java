package org.RoaringTreeMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.roaringbitmap.IntIterator;
import org.roaringbitmap.RoaringBitmap;

public class Tree {
	private BitmapTreeMap<Integer, RoaringBitmap> tree;
	
	public Tree() {
		tree = new BitmapTreeMap<Integer, RoaringBitmap>();
	}
	
	public final BitmapTreeMap<Integer, RoaringBitmap> getTree(){
		return tree;
	}
	
	/**
     * Returns the number of key-value mappings in this map.
     *
     * @return the number of key-value mappings in this map
     */
	public int size(){
		return tree.size();
	}
	
    /**
     * Generate a bitmap with the specified values set to true. The provided
     * integers values don't have to be in sorted order, but it may be
     * preferable to sort them from a performance point of view.
     *
     * @param dat set values
     * @return a new bitmap
     */
    public static Tree bitmapOf(final long... dat) {
        final Tree ans = new Tree();
        for (final long i : dat)
            ans.add(i);
        return ans;
    }
	
	/**
	 * Iterates over the mappings and the bitmap in each mapping to count the total number of integers in the tree.
	 * @return the total number of integers in this tree.
	 */
	public final long getCardinality() {
		Iterator<Entry<Integer, RoaringBitmap>> it = tree.entrySet().iterator();
		long card=0;
		while(it.hasNext())			
			card += it.next().getValue().getCardinality();
		return card;
	}
	
	public void putAll(Tree tr) {
		tree.putAll(tr.getTree());
	}
	
	public final void add(long integer) {
		int key = (int) (integer>>>32);
		int value = (int) integer;
		//Check if the key already exist
		RoaringBitmap rb = tree.get(key);
		if(rb!=null) {
			rb.add(value);
			return;
		}		
		//if key doesn't exist
		rb = new RoaringBitmap();
		rb.add(value);
		tree.put(new Integer(key), rb);
	}
	
	public final boolean contains(long integer){
		int key = (int) (integer>>>32);
		RoaringBitmap rb = tree.get(key);
		if(rb!=null)
			return rb.contains((int)integer);
		return false;	
	}
	
	public static Tree union(Tree tr1, Tree tr2) {
		Tree answer, small, big;		
		if(tr1.tree.size()<=tr2.tree.size()) {
			small=tr1;
			big=tr2;
		}
		else{
			small=tr2;
			big=tr1;
		}
		int sizeSmall= small.tree.size(), sizeBig= big.tree.size();
		if(sizeSmall*log2(sizeBig)<(sizeSmall+sizeBig))
			return answer= linearithmicUnion(small, big);			
		return answer= linearUnion(small, big);		
	}
	
	public static int log2( int value )
	{
	    int base = 0;
	    if((value & 0xffff0000 ) != 0 ) { value >>>= 16; base = 16; }
	    if( value >= 256 ) { value >>>= 8; base += 8; }
	    if( value >= 16  ) { value >>>= 4; base += 4; }
	    if( value >= 4   ) { value >>>= 2; base += 2; }
	    return base + ( value >>> 1 );
	}
	
	/**
	 * Perform a union between two trees in an O(n2 + n1 log(n1+n2)) time, where n1 is the size of the smallest tree.
	 * We don't consider the time to perform logical and between bitmaps in the big O notation.
	 * It is supposed that the first tree in input is smaller or equal to the second one.
	 * 
	 * @param tr1 the first tree
	 * @param tr2 the second tree
	 * @return a new tree containing the result of the two trees given in input
	 */
	public final static Tree linearithmicUnion(Tree tr1, Tree tr2) {
		//System.out.println("Linearithmic union");
		Tree answer = new Tree();	
		answer.tree.putAll(tr2.tree); //O(n2) time
		answer.tree.putAllByOr(tr1.tree); //O(n1 log(n1+n2)) time 			
		return answer;
	}
	
	/**
	 *  Perform a union between 2 trees in O(n1+n2)log(n1+n2) time, where n1 and n2 are the two trees sizes respectively.
	 * 	We don't consider the time to perform logical and between bitmaps in the big O notation.
	 * 
	 * @param tr first TreeMap
	 * @param tr2 second TreeMap
	 */
	public final static Tree linearUnion(Tree tr1, Tree tr2) {
		//System.out.println("Linear union ");		
		Tree answer = new Tree();
		Iterator<Entry<Integer, RoaringBitmap>> it1 = tr1.tree.entrySet().iterator();
		Iterator<Entry<Integer, RoaringBitmap>> it2 = tr2.tree.entrySet().iterator();		
		Entry<Integer, RoaringBitmap> e1=null, e2=null;
		int key1=0, key2 = 0;
		if(it1.hasNext() && it2.hasNext()){
			e1 = it1.next();
			e2 = it2.next();
			key1=e1.getKey(); 
			key2=e2.getKey();
			while(true) {
				if(key1<key2){	
					answer.tree.put(new Integer(key1), e1.getValue().clone());
					if(!it1.hasNext()){
						answer.tree.put(new Integer(key2), e2.getValue().clone());						
						break;
					}
					e1=it1.next();
					key1=e1.getKey();					
				}
				else if(key2<key1){
					answer.tree.put(new Integer(key2), e2.getValue().clone());
					if(!it2.hasNext()){
						answer.tree.put(new Integer(key1), e1.getValue().clone());						
						break;
					}
					e2=it2.next();
					key2=e2.getKey();					
				}
				else {
					RoaringBitmap rb = RoaringBitmap.or(e1.getValue(), e2.getValue());
					answer.tree.put(new Integer(key1), rb);
					if(!it1.hasNext() || !it2.hasNext())
						break;
					e1=it1.next();
					key1=e1.getKey();
					e2=it2.next();
					key2=e2.getKey();
				}			
			}
		}
		while(it1.hasNext()){
			e1=it1.next();
			answer.tree.put(new Integer(e1.getKey()), e1.getValue().clone());
		}
		while(it2.hasNext()){
			e2=it2.next();
			answer.tree.put(new Integer(e2.getKey()), e2.getValue().clone());
		}		
		return answer;
	}
	
	/**
	 * Perform a union between 2 trees in O(n1+n2) time, where n1 and n2 are the two trees sizes respectively.
	 * We don't consider the time to perform logical or between bitmaps in the big O notation.
	 * 
	 * @param tr first TreeMap
	 * @param tr2 second TreeMap
	 */
	public final static Tree linearUnionWithArray(Tree tr1, Tree tr2) {
		Tree answer = new Tree();
		Iterator<Entry<Integer, RoaringBitmap>> it1 = tr1.tree.entrySet().iterator();
		Iterator<Entry<Integer, RoaringBitmap>> it2 = tr2.tree.entrySet().iterator();	
		ArrayList<Entry<Integer, RoaringBitmap>> al = new ArrayList<Entry<Integer, RoaringBitmap>>();
		Entry<Integer, RoaringBitmap> e1=null, e2=null;
		int key1=0, key2 = 0;
		if(it1.hasNext() && it2.hasNext()){
			e1 = it1.next();
			e2 = it2.next();
			key1=e1.getKey(); 
			key2=e2.getKey();		
			while(true) {						
				if(key1<key2){
					al.add(new BitmapTreeMap.Entry<Integer, RoaringBitmap>(new Integer(key1), e1.getValue().clone(), null));
					if(!it1.hasNext())
						break;
					e1=it1.next();
					key1=e1.getKey();					
				}
				else if(key2<key1){
					al.add(new BitmapTreeMap.Entry<Integer, RoaringBitmap>(new Integer(key2), e2.getValue().clone(), null));
					if(!it2.hasNext())
						break;
					e2=it2.next();
					key2=e2.getKey();					
				}
				else {
					RoaringBitmap rb = RoaringBitmap.or(e1.getValue(), e2.getValue());
					al.add(new BitmapTreeMap.Entry<Integer, RoaringBitmap>(new Integer(key1), rb, null));
					if(!it1.hasNext() || !it2.hasNext())
						break;
					e1=it1.next();
					key1=e1.getKey();
					e2=it2.next();
					key2=e2.getKey();
				}			
			}
		}
		while(it1.hasNext()){
			e1=it1.next();			
			al.add(new BitmapTreeMap.Entry<Integer, RoaringBitmap>(new Integer(e1.getKey()), e1.getValue().clone(), null));
		}
		while(it2.hasNext()){
			e2=it2.next();
			al.add(new BitmapTreeMap.Entry<Integer, RoaringBitmap>(new Integer(e2.getKey()), e2.getValue().clone(), null));
		}		
		try {
			answer.tree.setRoot(buildFromSorted(0, 0, al.size()-1, BitmapTreeMap.computeRedLevel(al.size()),
			        al.iterator(), null, null));
		} catch (ClassNotFoundException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();}
		
		return answer;
	}
	
    /**
     * Recursive "helper method" that does the real work of the
     * previous method.  Identically named parameters have
     * identical definitions.  Additional parameters are documented below.
     * It is assumed that the comparator and size fields of the TreeMap are
     * already set prior to calling this method.  (It ignores both fields.)
     *
     * @param level the current level of tree. Initial call should be 0.
     * @param lo the first element index of this subtree. Initial should be 0.
     * @param hi the last element index of this subtree.  Initial should be
     *        size-1.
     * @param redLevel the level at which nodes should be red.
     *        Must be equal to computeRedLevel for tree of this size.
     */
    private final static BitmapTreeMap.Entry<Integer,RoaringBitmap> buildFromSorted(int level, int lo, int hi,
                                             int redLevel,
                                             Iterator it,
                                             java.io.ObjectInputStream str,
                                             RoaringBitmap defaultVal)
        throws  java.io.IOException, ClassNotFoundException {
        /*
         * Strategy: The root is the middlemost element. To get to it, we
         * have to first recursively construct the entire left subtree,
         * so as to grab all of its elements. We can then proceed with right
         * subtree.
         *
         * The lo and hi arguments are the minimum and maximum
         * indices to pull out of the iterator or stream for current subtree.
         * They are not actually indexed, we just proceed sequentially,
         * ensuring that items are extracted in corresponding order.
         */

        if (hi < lo) 
        	return null;

        int mid = (lo + hi) >>> 1;

        BitmapTreeMap.Entry<Integer,RoaringBitmap> left  = null;
        if (lo < mid)
            left = buildFromSorted(level+1, lo, mid - 1, redLevel,
                                   it, str, defaultVal);

        // extract key and/or value from iterator or stream
        Integer key;
        RoaringBitmap value;
        if (defaultVal==null) {
           Map.Entry<Integer,RoaringBitmap> entry = (Map.Entry<Integer,RoaringBitmap>)it.next();
           key = entry.getKey();
           value = entry.getValue();
        } else {
           key = null;
           value = defaultVal;
          }

        BitmapTreeMap.Entry<Integer,RoaringBitmap> middle =  
        		new BitmapTreeMap.Entry<>(new Integer(key), ((RoaringBitmap)value).clone(), null);

        // color nodes in non-full bottommost level red
        if (level == redLevel)
            middle.color = BitmapTreeMap.RED;

        if (left != null) {
            middle.left = left;
            left.parent = middle;
        }

        if (mid < hi) {
            BitmapTreeMap.Entry<Integer,RoaringBitmap> right = buildFromSorted(level+1, mid+1, hi, redLevel,
                                               it, str, defaultVal);
            middle.right = right;
            right.parent = middle;
        }

        return middle;
    }
	
	public final static Tree intersection(Tree tr1, Tree tr2) {
		Tree answer, small, big;		
		if(tr1.tree.size()<=tr2.tree.size()){
			small=tr1;
			big=tr2;
		}
		else{
			small=tr2;
			big=tr1;
		}		
		int sizeSmall=small.tree.size(), sizeBig=big.tree.size();
		if(sizeSmall*(Math.log(sizeBig)/Math.log(2))<(sizeSmall+sizeBig))
			return answer= linearithmicIntersect(small, big);			
		return answer= linearIntersectWithArray(small, big);				
	}
	
	/**
	 * Iterates over the first TreeMap removing mappings not shared with the second TreeMap and intersecting values of
	 * shared mappings with the second treeMap. Complexity of this method is O(n1 log(n2)), where n1 is the number of
	 * mappings in the first TreeMap, and n2 is the number of mappings in the second TreeMap. 
	 * It is supposed that the first tree's size <= second tree's size.
	 * We don't consider the time to perform logical and between bitmaps in the big O notation.
	 *  
	 * @param tr first TreeMap
	 * @param tr2 second TreeMap
	 */
	public final static Tree linearithmicIntersect(Tree tr1, Tree tr2){
		Tree answer = new Tree();			
		Iterator<Entry<Integer, RoaringBitmap>> it = tr1.tree.entrySet().iterator();
		while(it.hasNext()) {
			Entry<Integer, RoaringBitmap> e = it.next();
			RoaringBitmap rb = tr2.tree.get(e.getKey());
			if(rb!=null) {
				rb=RoaringBitmap.and(rb, e.getValue());
				if(!rb.isEmpty())					 					
					answer.tree.put(new Integer(e.getKey()), rb);
			}
		}
		return answer;
	}		
	
	/**
	 *  Perform an intersection between 2 trees in O(n1+n2) time, where n1 and n2 are the two trees sizes respectively.
	 * 	We don't consider the time to perform logical and between bitmaps in the big O notation.
	 * 
	 * @param tr1 first TreeMap
	 * @param tr2 second TreeMap
	 */
	public final static Tree linearIntersect(Tree tr1, Tree tr2) {
		Tree answer = new Tree();
		Iterator<Entry<Integer, RoaringBitmap>> it1 = tr1.tree.entrySet().iterator();
		Iterator<Entry<Integer, RoaringBitmap>> it2 = tr2.tree.entrySet().iterator();		
		Entry<Integer, RoaringBitmap> e1=null, e2=null;
		int key1=0, key2 = 0;
		if(it1.hasNext() && it2.hasNext()){
			e1 = it1.next();
			e2 = it2.next();
			key1=e1.getKey(); 
			key2=e2.getKey();		
			while(true) {			
				//nbIt++;
				if(key1<key2){
					if(!it1.hasNext())
						break;
					e1=it1.next();
					key1=e1.getKey();
				}
				else if(key2<key1){
					if(!it2.hasNext())
						break;
					e2=it2.next();
					key2=e2.getKey();					
				}
				else {
					RoaringBitmap rb = RoaringBitmap.and(e1.getValue(), e2.getValue());				
					if(!rb.isEmpty())					 					
						answer.tree.put(new Integer(key1), rb);
					if(!it1.hasNext() || !it2.hasNext())
						break;
					e1=it1.next();
					key1=e1.getKey();
					e2=it2.next();
					key2=e2.getKey();
				}			
			}
		}
		//System.out.println("nb linear iter = "+nbIt);
		return answer;
	}
	
	/**
	 * Perform an intersection between 2 RoaringTreeMaps in O(n1+n2) time, where n1 and n2 are the two trees sizes respectively.
	 * We don't consider the time to perform logical or between bitmaps in the big O notation.
	 * 
	 * @param tr first TreeMap
	 * @param tr2 second TreeMap
	 */
	public final static Tree linearIntersectWithArray(Tree tr1, Tree tr2) {
		Tree answer = new Tree();
		Iterator<Entry<Integer, RoaringBitmap>> it1 = tr1.tree.entrySet().iterator();
		Iterator<Entry<Integer, RoaringBitmap>> it2 = tr2.tree.entrySet().iterator();	
		ArrayList<Entry<Integer, RoaringBitmap>> al = new ArrayList<Entry<Integer, RoaringBitmap>>();
		Entry<Integer, RoaringBitmap> e1=null, e2=null;
		int key1=0, key2 = 0;
		if(it1.hasNext() && it2.hasNext()){
			e1 = it1.next();
			e2 = it2.next();
			key1=e1.getKey(); 
			key2=e2.getKey();		
			while(true) {						
				if(key1<key2){
					if(!it1.hasNext())
						break;
					e1=it1.next();
					key1=e1.getKey();					
				}
				else if(key2<key1){
					if(!it2.hasNext())
						break;
					e2=it2.next();
					key2=e2.getKey();					
				}
				else {
					RoaringBitmap rb = RoaringBitmap.and(e1.getValue(), e2.getValue());
					if(!rb.isEmpty())
						al.add(new BitmapTreeMap.Entry<Integer, RoaringBitmap>(new Integer(key1), rb, null));
					if(!it1.hasNext() || !it2.hasNext())
						break;
					e1=it1.next();
					key1=e1.getKey();
					e2=it2.next();
					key2=e2.getKey();
				}			
			}
		}		
		try {
			answer.tree.setRoot(buildFromSorted(0, 0, al.size()-1, BitmapTreeMap.computeRedLevel(al.size()),
			        al.iterator(), null, null));
		} catch (ClassNotFoundException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();}
		
		return answer;
	}
	
	public final Object[] toArray() {
		ArrayList<Long> array = new ArrayList<Long>(); 
		Iterator<Entry<Integer, RoaringBitmap>> it = tree.entrySet().iterator();		
		while(it.hasNext()){
			Entry<Integer, RoaringBitmap> e = it.next();
			int key = e.getKey();
			IntIterator rit = e.getValue().getIntIterator();
			while(rit.hasNext()){
				long val= (UtilRoaringTreeMap.intToLongUnsigned(key)<<32)|
						UtilRoaringTreeMap.intToLongUnsigned(rit.next()); 
				array.add(val);				
			}
		}
		return array.toArray();
	}

	public void remove(long x) {
		int key = (int)(x>>>32);
		int value= (int)x;
		RoaringBitmap rb= tree.get(key);
		if(rb!=null) {
			rb.remove(value);
			if(rb.isEmpty())
				tree.remove(key);					
		}
	}	
}
